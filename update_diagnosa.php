<?php
require_once "koneksi.php";

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $response = array();

    $username = $_POST['username'];

    $query =

        "SELECT A.id_examinations, A.keluhan, A.images_camera,
    B.name,
    B.profilePicture
FROM examinations A
    LEFT JOIN users B ON B.name = A.patient_name
WHERE A.doctor_username = '$username'
    AND (
        A.checked = 0
        OR A.checked = 2
    ) ORDER BY A.updateAt ASC";

    $hasil = mysqli_query($con, $query);

    if (!$hasil) {
        http_response_code(666);
        echo (mysqli_error($con));
    }

    if (isset($hasil)) {
        while ($data = mysqli_fetch_array($hasil, MYSQLI_ASSOC)) {
            $response[] = $data;
        };
    } else {
        http_response_code(505);
        $response = ['status' => "error", "code" => "505, Unknown Error"];
    }

    echo json_encode($response);
}
