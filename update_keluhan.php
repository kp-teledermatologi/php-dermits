<?php
require_once "koneksi.php";

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $response = array();

    $user_id = $_POST['user_id'];

    $query =

        "SELECT A.id_examinations,
    A.images_camera,
    A.diagnoses_description,
    A.diagnoses_disease,
    B.name,
    B.hospital,
    B.profilePicture
FROM examinations A
    LEFT JOIN users B ON B.username = A.doctor_username
WHERE A.patient_code = '$user_id'
    AND (
        A.checked = 1
        OR A.checked = 3
    ) ORDER BY A.updateAt DESC";

    $hasil = mysqli_query($con, $query);

    if (!$hasil) {
        http_response_code(666);
        echo (mysqli_error($con));
    }

    if (isset($hasil)) {
        while ($data = mysqli_fetch_array($hasil, MYSQLI_ASSOC)) {
            $response[] = $data;
        };
    } else {
        http_response_code(505);
        $response = ['status' => "error", "code" => "505, Unknown Error"];
    }

    echo json_encode($response);
}
