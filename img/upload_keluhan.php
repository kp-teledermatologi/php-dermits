<?php
require "../koneksi.php";

if($_SERVER['REQUEST_METHOD'] == "POST"){
    $response = array();
    
    //variable langsung ditentukan di PHP
    $checked = 0;
    $createdAt = date("Y-m-d");
    $updateAt = date("Y-m-d");
    
    //variable random dari db
    $query= "SELECT * FROM `users` WHERE `role`= '1' ORDER BY RAND() LIMIT 1";
    $hasil_dokter = mysqli_query($con, $query);
    $row = mysqli_fetch_assoc($hasil_dokter); 
    $doctor_name = $row["name"];
    $doctor_username = $row["username"];
    $clinic_name = $row["hospital"];
    

    //variable ambil dari app
    $patient_name = $_POST['patient_name'];
    $patient_code = $_POST['patient_code'];
    $keluhan = $_POST['keluhan'];
    $image = $_POST['image'];
    $images_camera = $_POST['images_camera'];
    
    //upload image ke server
    $path_server_save = 'keluhan/'.$images_camera;
    $realImage = base64_decode($image);
    file_put_contents($path_server_save, $realImage);

    //path image
    $path_image = 'img/keluhan/'.$images_camera;

    // query SQL untuk insert data
    $query_insert="INSERT INTO `examinations` SET `patient_name`='$patient_name',`patient_code`='$patient_code',
    `images_camera`='$path_image',`keluhan`='$keluhan', `checked` = '$checked',`createdAt`='$createdAt',
    `updateAt`='$updateAt', `doctor_name`='$doctor_name',`doctor_username`='$doctor_username',`clinic_name`='$clinic_name'";

    $hasil = mysqli_query($con, $query_insert);
        if(isset($hasil)){
            $response['value']   = '1';
            echo json_encode($response);
        } else{
                $response['value']   = '0';
                echo json_encode($response);
        }
    }
?>