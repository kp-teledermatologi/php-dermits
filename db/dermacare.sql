-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 18 Jul 2020 pada 10.55
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dermacare`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `conversations`
--

CREATE TABLE `conversations` (
  `id_conversations` int(11) NOT NULL,
  `members_userId` int(45) NOT NULL,
  `messages_sender` int(45) NOT NULL,
  `messages_message` varchar(45) NOT NULL,
  `createdAt` date NOT NULL,
  `updateAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `examinations`
--

CREATE TABLE `examinations` (
  `id_examinations` int(11) NOT NULL,
  `checked` tinyint(1) NOT NULL,
  `doctor_name` varchar(45) NOT NULL,
  `doctor_username` varchar(45) NOT NULL,
  `doctor_hospital` varchar(45) DEFAULT NULL,
  `patient_name` varchar(45) NOT NULL,
  `patient_code` varchar(45) NOT NULL,
  `clinic_name` varchar(45) NOT NULL,
  `clinic_username` varchar(45) DEFAULT NULL,
  `clinic_officer` varchar(45) DEFAULT NULL,
  `images_microscopic` varchar(45) DEFAULT NULL,
  `images_camera` varchar(100) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `keluhan` varchar(144) NOT NULL,
  `results_auto_class` varchar(45) DEFAULT NULL,
  `result_auto_probability` int(11) DEFAULT NULL,
  `result_manual` varchar(45) DEFAULT NULL,
  `diagnoses_description` varchar(144) DEFAULT NULL,
  `diagnoses_cost` int(11) DEFAULT NULL,
  `diagnoses_disease` varchar(45) DEFAULT NULL,
  `diagnoses_recipes_default` varchar(45) DEFAULT NULL,
  `diagnoses_recipe_type_medicine` varchar(45) DEFAULT NULL,
  `diagnoses_recipe_type_description` varchar(45) DEFAULT NULL,
  `diagnoses_recipe_type_usage` varchar(45) DEFAULT NULL,
  `createdAt` date NOT NULL,
  `updateAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `examinations`
--

INSERT INTO `examinations` (`id_examinations`, `checked`, `doctor_name`, `doctor_username`, `doctor_hospital`, `patient_name`, `patient_code`, `clinic_name`, `clinic_username`, `clinic_officer`, `images_microscopic`, `images_camera`, `type`, `keluhan`, `results_auto_class`, `result_auto_probability`, `result_manual`, `diagnoses_description`, `diagnoses_cost`, `diagnoses_disease`, `diagnoses_recipes_default`, `diagnoses_recipe_type_medicine`, `diagnoses_recipe_type_description`, `diagnoses_recipe_type_usage`, `createdAt`, `updateAt`) VALUES
(1, 0, 'dr. Tatan', '@tatan', NULL, 'Marfuah', '1', 'Klinik Tong Fang', NULL, NULL, NULL, 'http://10.0.2.2/dermacare/img/keluhan/marfuah.jpg', NULL, 'Gatal gatal dan bengkak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-14', '2020-07-14'),
(2, 1, 'dr. Tatan', '@tatan', NULL, 'Marfuah', '1', 'Klinik Tong Fang', NULL, NULL, NULL, 'http://10.0.2.2/dermacare/img/keluhan/marimar.jpg', NULL, 'Bengkak sakit', NULL, NULL, NULL, 'Jerawat adalah blablablabla', NULL, 'Jerawat', NULL, 'sakatonik, obh', NULL, '3x1, 2x1', '2020-07-14', '2020-07-14'),
(3, 2, 'dr. Ajudan Pribadi', '@ajudanpribadi', 'RS Padang Sederhana', 'Marfuah', '1', 'Klinik Tong Fang', NULL, NULL, NULL, 'http://10.0.2.2/dermacare/img/keluhan/marfuah.jpg', NULL, 'Seperti mati lampu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-16', '2020-07-16'),
(4, 3, 'dr. Ajudan Pribadi', '@ajudanpribadi', 'RS Padang Sederhana', 'Marfuah', '1', 'Klinik Tong Fang', NULL, NULL, NULL, 'http://10.0.2.2/dermacare/img/keluhan/marfuah.jpg', NULL, 'Sakit kulit', NULL, NULL, NULL, 'Dermatitis adalah wakwakwkwkawkaaksokaow', NULL, 'Dermatitis atopik', NULL, 'panadol, paramex, promag', NULL, '1x1, 2x1, 3x1', '2020-07-16', '2020-07-16'),
(5, 1, 'dr. Tatan', '@tatan', NULL, 'Marfuah', '1', 'Klinik Tong Fang', NULL, NULL, NULL, 'http://10.0.2.2/dermacare/img/keluhan/percobaan4.png', NULL, 'panas dingin award', NULL, NULL, NULL, 'Penyakit siput gila adalah penyakit yang sangat aneh', NULL, 'Penyakit Siput Gila', NULL, 'paracetamol, domperidone, kalxetin', NULL, '3x1, 2x1, 1x1', '2020-07-17', '2020-07-18'),
(6, 2, 'dr. Ajudan Pribadi', '@ajudanpribadi', 'RS Padang Sederhana', 'Marfuah', '1', 'Klinik Tong Fang', NULL, NULL, NULL, 'http://10.0.2.2/dermacare/img/keluhan/foto_2020-07-17 12:02:00.935484.jpg', NULL, 'kri cuy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-17', '2020-07-18'),
(7, 0, 'dr. Tatan', '@tatan', NULL, 'Marfuah', '1', 'Klinik Tong Fang', NULL, NULL, NULL, 'http://10.0.2.2/dermacare/img/keluhan/1_17-07-2020.jpg', NULL, 'workshop kibm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-17', '2020-07-17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hospitals`
--

CREATE TABLE `hospitals` (
  `id_hospital` int(11) NOT NULL,
  `image` varchar(45) NOT NULL,
  `about` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `website` varchar(45) NOT NULL,
  `createdAt` date NOT NULL,
  `updateAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `liveInterractiveSubmission`
--

CREATE TABLE `liveInterractiveSubmission` (
  `id_live` int(11) NOT NULL,
  `patient_id` varchar(45) NOT NULL,
  `patient_nik` varchar(45) NOT NULL,
  `patient_name` varchar(45) NOT NULL,
  `clinic_name` varchar(45) NOT NULL,
  `clinic_username` varchar(45) NOT NULL,
  `clinic_officer` varchar(45) NOT NULL,
  `accepted` tinyint(1) NOT NULL,
  `hospital` varchar(45) NOT NULL,
  `responses_default` varchar(45) NOT NULL,
  `responses_type_comment` varchar(45) NOT NULL,
  `responses_type_doctor_name` varchar(45) NOT NULL,
  `responses_type_doctor_username` varchar(45) NOT NULL,
  `responses_type_createdAt` varchar(45) NOT NULL,
  `createdAt` date NOT NULL,
  `updateAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `profilePicture` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  `identityNumber` varchar(45) NOT NULL,
  `dob` date NOT NULL,
  `hospital` varchar(45) DEFAULT NULL,
  `officers_name` varchar(45) DEFAULT NULL,
  `officer_gender` varchar(45) DEFAULT NULL,
  `officer_dob` varchar(45) DEFAULT NULL,
  `officer_identityNumber` varchar(45) DEFAULT NULL,
  `patients_phone` varchar(45) DEFAULT NULL,
  `patients_name` varchar(45) DEFAULT NULL,
  `patiens_dob` date DEFAULT NULL,
  `patients_nik` varchar(45) DEFAULT NULL,
  `patients_addres` varchar(45) DEFAULT NULL,
  `patients_profilePicture` varchar(45) DEFAULT NULL,
  `tokens` varchar(45) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedAt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `name`, `email`, `password`, `profilePicture`, `phone`, `username`, `role`, `identityNumber`, `dob`, `hospital`, `officers_name`, `officer_gender`, `officer_dob`, `officer_identityNumber`, `patients_phone`, `patients_name`, `patiens_dob`, `patients_nik`, `patients_addres`, `patients_profilePicture`, `tokens`, `createdAt`, `updatedAt`) VALUES
(1, 'Marfuah', 'marfuah@its.ac.id', '1234', 'http://10.0.2.2/dermacare/img/profile/1.jpg', '08181818', '@marfuah', '0', '1982913728738', '1999-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'dr. Tatan', 'tatan@tatan.com', '1234', 'http://10.0.2.2/dermacare/img/profile/2.jpg', '081129102', '@tatan', '1', '1239298329123', '1964-07-23', 'Klinik Tong Fang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'dr. Ajudan Pribadi', 'ajud@ajud.com', '1234', 'http://10.0.2.2/dermacare/img/profile/3.jpg', '0812192183', '@ajudanpribadi', '2', '12931237143434', '1989-08-27', 'RS Padang Sederhana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id_conversations`);

--
-- Indeks untuk tabel `examinations`
--
ALTER TABLE `examinations`
  ADD PRIMARY KEY (`id_examinations`);

--
-- Indeks untuk tabel `hospitals`
--
ALTER TABLE `hospitals`
  ADD PRIMARY KEY (`id_hospital`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id_conversations` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `examinations`
--
ALTER TABLE `examinations`
  MODIFY `id_examinations` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `hospitals`
--
ALTER TABLE `hospitals`
  MODIFY `id_hospital` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
