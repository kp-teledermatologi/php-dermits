<?php
    require "koneksi.php";
    
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $response = array();
        $patient_code = $_POST['patient_code'];

        $query = "SELECT A.id_examinations, A.checked, A.doctor_name, A.doctor_username, A.doctor_hospital, A.clinic_name, A.createdAt, B.profilePicture
                    FROM examinations A, users B
                    WHERE A.doctor_username = B.username
                    AND A.patient_code = '$patient_code'";
        $hasil = mysqli_query($con,$query);

        if (!$hasil) {
            printf("Error: %s\n", mysqli_error($con));
            exit();
        }
        if(isset($hasil)){
            while ($fetchdata = mysqli_fetch_array($hasil)){
                $response[] = $fetchdata;      
            }
            echo json_encode($response);
        }
        else{
            echo json_decode('ERROR');
        }
    
    }
?>