<?php
    require "koneksi.php";
    
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $response = array();
        $doctor_username = $_POST['doctor_username'];

        $query = "SELECT A.id_examinations, A.checked, A.doctor_name, A.patient_name, A.patient_code, A.doctor_hospital, A.clinic_name, A.createdAt, B.profilePicture
                    FROM examinations A, users B
                    WHERE A.patient_name = B.name
                    AND A.doctor_username = '$doctor_username'";
        $hasil = mysqli_query($con,$query);

        if (!$hasil) {
            printf("Error: %s\n", mysqli_error($con));
            exit();
        }
        if(isset($hasil)){
            while ($fetchdata = mysqli_fetch_array($hasil)){
                $response[] = $fetchdata;      
            }
            echo json_encode($response);
        }
        else{
            echo json_decode('ERROR');
        }
    
    }
?>