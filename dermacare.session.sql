SELECT A.*,
    B.name,
    B.profilePicture
FROM examinations A
    LEFT JOIN users B ON B.username = A.patient_name
WHERE A.doctor_username = '@tatan'
    AND (
        A.checked = 0
        OR A.checked = 2
    )
ORDER BY A.updateAt ASC