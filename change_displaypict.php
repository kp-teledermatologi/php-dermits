<?php

require_once "koneksi.php";

$response = array();

$user_id = $_POST['user_id'];
$password = $_POST['password'];

switch ($_FILES['displaypic']['error']) {
    case UPLOAD_ERR_OK:
        break;
    case UPLOAD_ERR_NO_FILE:
        echo "no file sent";
        throw new RuntimeException('No file sent.');
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
        echo "TOO BIIIG";
        throw new RuntimeException('Exceeded filesize limit.');
    default:
        echo "Unknown Error";
        throw new RuntimeException('Unknown errors.');
}

//checking if the files is image or not
if (exif_imagetype($_FILES['displaypic']['tmp_name'])) {
    $uploaddir = 'img/profile/';
    $targetdir = $uploaddir . $user_id . '.' . pathinfo($_FILES['displaypic']['name'], PATHINFO_EXTENSION);

    $query = "SELECT * FROM `users` where `id_user`='$user_id' and `password` = '$password'";
    $hasil = mysqli_query($con, $query);

    if (!$hasil) {
        echo (mysqli_error($con));
    }

    /**
     * kalau $hasil hanya berisi satu user saja ( yang berarti password dan user_id sudah benar )
     * pindahkan file dari temp ke uploaded_img dan simpan path nya ke db
     */
    if (mysqli_num_rows($hasil) == 1) {
        if (move_uploaded_file($_FILES['displaypic']['tmp_name'], $targetdir)) {
            $change_query = "UPDATE `users` SET `profilePicture`='$targetdir' WHERE `id_user`='$user_id'";
            $hasil = mysqli_query($con, $change_query);
            $response = ['status' => "OK", "code" => "200, Request Sukses"];
        } else {
            $response = ['status' => "error", "code" => "502, Password Salah"];
        }

        if (!isset($response)) {
            $response = ['status' => "error", "code" => "505, Unknown Error, change displaypic"];
        }
    }
} else {
    $response = ['status' => "error", "code" => "666, Not an Image File"];
}


echo json_encode($response);
